# Fun Fact Quiz Module

This module allows you to create and manage a fun fact quiz on your Drupal website. The module adds a menu item "Fun Fact Quiz":

- Add Quiz: This menu item takes you to a form where you can add a new quiz to the database.

- Show Quiz: This menu item displays a random quiz from the database.

You can also add quizzes by visiting the URL /add-question.

## Installation

To install the Fun Fact Quiz module, follow these steps:

1. Copy the "fun_fact_quiz" folder to your Drupal modules directory.
2. Go to your Drupal website's "Extend" page and enable the module.
3. Visit the "Fun Fact Quiz" menu on your Drupal website to use the module.

## Requirements

This module requires Drupal 8 or higher.

## Usage

To add a new quiz, click on the "Add Quiz" menu item and fill out the form with the quiz questions and answers.

To manage existing quizzes, click on the "Fun Fact Quiz" menu item and select the quiz you want to edit, update, or delete.

To display a random quiz, click on the "Show Quiz" menu item or can go to /fun-fact-quiz.

You can also add quizzes by visiting the URL /add-question.

## Credits

This module was created by Sandeep Kumar for LN Webworks PVT LTD .
