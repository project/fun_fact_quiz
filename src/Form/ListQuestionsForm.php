<?php

namespace Drupal\fun_fact_quiz\Form;

use Drupal\Core\Link;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class ListQuestionsForm.
 *
 * @package Drupal\fun_fact_quiz\Form
 */
class ListQuestionsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fun_fact_quiz_list_questions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Add the "Add Quiz" button.
    $form['add_quiz_button'] = [
      '#type' => 'link',
      '#title' => $this->t('Add Quiz'),
      '#url' => Url::fromRoute('fun_fact_quiz.add_question'),
      '#attributes' => ['class' => ['button', 'button-action']],
    ];

    // Add the "Show Quiz" button.
    $form['Show_quiz_button'] = [
      '#type' => 'link',
      '#title' => $this->t('Show Quiz'),
      '#url' => Url::fromRoute('fun_fact_quiz.quiz'),
      '#attributes' => ['class' => ['button', 'button-action']],
      '#cache' => [
        'tags' => ['my_custom_cache_tag'],
      ],
    ];

    // Get the list of questions from the database.
    $database = \Drupal::database();
    $query = $database->select('quiz_questions', 'qq')
      ->fields('qq', ['id', 'question', 'answer1', 'answer2', 'answer3', 'correct_answer'])
      ->orderBy('id');
    $result = $query->execute();

    // Add a table to display the list of questions.
    $header = [
      'id' => $this->t('ID'),
      'question' => $this->t('Question'),
      'answer1' => $this->t('Answer 1'),
      'answer2' => $this->t('Answer 2'),
      'answer3' => $this->t('Answer 3'),
      'correct_answer' => $this->t('Correct Answer'),
      'actions' => $this->t('Actions'),
      'delete' => $this->t('Actions'),
    ];

    $rows = [];
    foreach ($result as $row) {
      $id = $row->id;
      // dump($id);
      $rows[$id]['id'] = $id;
      $rows[$id]['question'] = $row->question;
      $rows[$id]['answer1'] = $row->answer1;
      $rows[$id]['answer2'] = $row->answer2;
      $rows[$id]['answer3'] = $row->answer3;
      $rows[$id]['correct_answer'] = $row->correct_answer;
      $rows[$id]['actions'] = Link::createFromRoute($this->t('Edit'), 'fun_fact_quiz.add_question', ['qid' => $id])->toString();
      $rows[$id]['delete'] = Link::createFromRoute($this->t('Delete'), 'fun_fact_quiz.delete_question', ['qid' => $id])->toString();
    }

    $form['questions_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No questions found.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
