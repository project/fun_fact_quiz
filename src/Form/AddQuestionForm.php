<?php

namespace Drupal\fun_fact_quiz\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class AddQuestionForm.
 *
 * @package Drupal\fun_fact_quiz\Form
 */
class AddQuestionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fun_fact_quiz_add_question_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the question ID from the URL parameters.
    $question_id = $this->getRequest()->get('qid');

    // If a question ID is present, load the existing question from the database.
    if ($question_id) {
      $query = \Drupal::database()->select('quiz_questions', 'q');
      $query->fields('q', ['question', 'answer1', 'answer2', 'answer3', 'correct_answer']);
      $query->condition('q.id', $question_id);
      $question = $query->execute()->fetchAssoc();
      // If (!$question) {
      //   drupal_set_message($this->t('Invalid question ID.'), 'error');
      //   $form_state->setRedirect('fun_fact_quiz.question_list');
      // }.
    }

    $form = [];

    $form['question'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Question'),
      '#required' => TRUE,
      '#default_value' => isset($question) ? $question['question'] : '',
    ];

    // Add three text fields for the answer options.
    $form['answer1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Answer option 1'),
      '#required' => TRUE,
      '#default_value' => isset($question) ? $question['answer1'] : '',
    ];

    $form['answer2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Answer option 2'),
      '#required' => TRUE,
      '#default_value' => isset($question) ? $question['answer2'] : '',
    ];

    $form['answer3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Answer option 3'),
      '#required' => TRUE,
      '#default_value' => isset($question) ? $question['answer3'] : '',
    ];

    // Add a text field for the correct answer.
    $form['correct_answer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Correct answer'),
      '#required' => TRUE,
      '#default_value' => isset($question) ? $question['correct_answer'] : '',
    ];

    // If an existing question is being edited, add the question ID as a hidden field.
    if ($question_id) {
      $form['question_id'] = [
        '#type' => 'hidden',
        '#value' => $question_id,
      ];
    }
    // Add a submit button to save the question.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => isset($question) ? $this->t('Save changes') : $this->t('Save question'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the values submitted by the form.
    $question = $form_state->getValue('question');
    $answer1 = $form_state->getValue('answer1');
    $answer2 = $form_state->getValue('answer2');
    $answer3 = $form_state->getValue('answer3');
    $correct_answer = $form_state->getValue('correct_answer');
    $question_id = $form_state->getValue('question_id');

    // Insert or update the question and answers in the database.
    $database = \Drupal::database();
    if ($question_id) {
      $database->update('quiz_questions')
        ->fields([
          'question' => $question,
          'answer1' => $answer1,
          'answer2' => $answer2,
          'answer3' => $answer3,
          'correct_answer' => $correct_answer,
        ])
        ->condition('id', $question_id)
        ->execute();

      // Display a message to confirm the question was updated.
      $messenger = \Drupal::messenger();
      $messenger->addMessage($this->t('Question updated successfully.'));
      // Redirect to the fun_fact_quiz.quiz route.
      $redirect_url = Url::fromRoute('fun_fact_quiz.list_questions');
      $response = new RedirectResponse($redirect_url->toString());
      $response->send();

    }
    else {
      $database->insert('quiz_questions')
        ->fields([
          'question' => $question,
          'answer1' => $answer1,
          'answer2' => $answer2,
          'answer3' => $answer3,
          'correct_answer' => $correct_answer,
        ])
        ->execute();

      // Display a message to confirm the question was added.
      $messenger = \Drupal::messenger();
      $messenger->addMessage($this->t('Question added successfully.'));
      // Redirect the user to the question list page.
      // Redirect to the fun_fact_quiz.quiz route.
      $redirect_url = Url::fromRoute('fun_fact_quiz.list_questions');
      $response = new RedirectResponse($redirect_url->toString());
      $response->send();

    }

  }

}
