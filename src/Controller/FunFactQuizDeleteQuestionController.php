<?php

namespace Drupal\fun_fact_quiz\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for deleting a question.
 */
class FunFactQuizDeleteQuestionController extends ControllerBase {

  /**
   * Deletes a question from the database.
   *
   * @param int $qid
   *   The ID of the question to be deleted.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the question list page.
   */
  public function deleteQuestion($qid) {
    // Delete the question from the database.
    $database = Database::getConnection();
    $database->delete('quiz_questions')
      ->condition('id', $qid)
      ->execute();

    // Display a message to confirm the question was deleted.
    $this->messenger()->addMessage($this->t('Question deleted successfully.'));

    // Redirect the user to the question list page.
    $url = Url::fromRoute('fun_fact_quiz.list_questions');
    return new RedirectResponse($url->toString());
  }

}
