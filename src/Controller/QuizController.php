<?php

namespace Drupal\fun_fact_quiz\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;

/**
 *
 */
class QuizController extends ControllerBase {

  /**
   *
   */
  public function view($qid) {
    $database = Database::getConnection();

    // Get the question data from the database.
    $query = $database->select('quiz_questions', 'qq')
      ->fields('qq', ['id', 'question', 'answer1', 'answer2', 'answer3', 'correct_answer'])
      ->condition('id', $qid)
      ->execute();
    $question = $query->fetchAssoc();

    // Create a table to display the question data.
    $header = [
      'id' => $this->t('ID'),
      'question' => $this->t('Question'),
      'answer1' => $this->t('Answer 1'),
      'answer2' => $this->t('Answer 2'),
      'answer3' => $this->t('Answer 3'),
      'correct_answer' => $this->t('Correct Answer'),
    ];

    $rows = [];
    if ($question) {
      $rows[] = [
        'id' => $question['id'],
        'question' => $question['question'],
        'answer1' => $question['answer1'],
        'answer2' => $question['answer2'],
        'answer3' => $question['answer3'],
        'correct_answer' => $question['correct_answer'],
      ];
    }

    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No question found.'),
    ];

    return [
      '#theme' => 'table',
      '#title' => $this->t('Question #@qid', ['@qid' => $qid]),
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No question found.'),
    ];
  }

}
