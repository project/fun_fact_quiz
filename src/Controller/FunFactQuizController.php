<?php

namespace Drupal\fun_fact_quiz\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class FunFactQuizController extends ControllerBase {

  /**
   *
   */
  public function quiz() {

    $database = \Drupal::database();
    $query = $database->select('quiz_questions', 'qq')
      ->fields('qq', ['question', 'answer1', 'answer2', 'answer3', 'correct_answer'])
      ->execute();
    $questions = [];
    $answers = [];
    $correct_answer = [];
    foreach ($query as $row) {
      $questions[] = $row->question;
      $answers[] = [$row->answer1, $row->answer2, $row->answer3];
      $correct_answer[] = $row->correct_answer;
    }

    // If there are no questions, display a message to the user.
    if (empty($questions)) {
      $build = [
        '#markup' => $this->t('Quiz not added yet. Please add quiz.'),
      ];
      return $build;
    }

    // Generate a random question and its answers.
    // $question_index = rand(0, count($questions) - 1);.
    $question_index = array_rand($questions);
    $question = $questions[$question_index];
    $answer_options = $answers[$question_index];
    $correct_answer = $correct_answer[$question_index];

    // Add "Show Answer" button to answer options.
    $answer_options[] = 'Show Answer';

    // Render the quiz template with the question and answer options.
    $build = [
      '#theme' => 'fun_fact_quiz',
      '#question' => $question,
      '#answer_options' => $answer_options,
      '#correct_answer' => $correct_answer,
      '#title' => $this->t('Fun Fact Quiz'),
    ];

    // Disable caching for this page.
    $build['#cache']['max-age'] = 0;
    $build['#cache']['no_cache'] = TRUE;

    // Add a cache tag for the quiz to ensure the question is regenerated on each request.
    $build['#cache']['tags'][] = 'quiz';
    // Clear cache for the quiz.
    return $build;
  }

}
